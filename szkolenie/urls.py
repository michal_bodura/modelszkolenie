"""szkolenie URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.http import HttpResponse
from django.urls import path, include
from modelszkolenie import views
from modelszkolenie.admin import my_admin_site

app_name = 'modelszkolenie'


def okay(request):
    return HttpResponse('pretend-binary-data-here', content_type='image/jpeg')

# Definicja adresów URL panelu administracyjnego oraz panelu REST API


urlpatterns = i18n_patterns(
    path('szkolenieadmin/', my_admin_site.urls),
    path('', views.HomeView.as_view(), name='home'),
    path('api/v1/company', views.CompanyList.as_view()),
    path('api/v1/company/<int:pk>', views.CompanyDetail.as_view(), name="currentCompany"),
    path('api/v1/user', views.UserList.as_view()),

    path('api/v1/user/<int:pk>', views.UserDetail.as_view(), name="currentUser"),
    # path('api/v1/user/<card_slug>', views.UserDetail.as_view()),
    path('api/v1/training', views.TrainingList.as_view()),
    path('api/v1/training/<int:pk>', views.TrainingDetail.as_view(), name="currentTraining"),
    path('api/v1/question', views.QuestionList.as_view()),
    path('api/v1/question/<int:pk>', views.QuestionDetail.as_view(), name="currentQuestion"),
    path('api/v1/answer', views.AnswerList.as_view()),
    path('api/v1/answer/<int:pk>', views.AnswerDetail.as_view(), name="currentAnswer"),
    path('api/v1/user/<int:pk>/trainings', views.TrainingUserList.as_view(), name="completedTraining"),
    # path('api/v1/user/<card_slug>/trainings',
    #      views.TrainingUserList.as_view()),
    path('api/v1/question/<int:pk>/answers',
         views.QuestionsAnswerList.as_view()),
    path('favicon.ico', okay),
    path('authenticate/', views.CustomObtainAuthToken.as_view()),

    prefix_default_language=False
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


"""
WSGI config for szkolenie project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""
from django.core.wsgi import get_wsgi_application
from os import environ as Env


Env.setdefault('DJANGO_SETTINGS_MODULE', 'szkolenie.settings')

application = get_wsgi_application()

"""
ASGI config for szkolenie project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""
from django.core.asgi import get_asgi_application
from os import environ as Env

Env.setdefault('DJANGO_SETTINGS_MODULE', 'szkolenie.settings')

application = get_asgi_application()

from .forms import *
<<<<<<< HEAD
from .models import *
from rest_framework.serializers import ModelSerializer, SerializerMethodField
=======
from django.utils.translation import gettext_lazy as _
from rest_framework.serializers import ModelSerializer, SerializerMethodField, IntegerField, CharField

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu Company istotnych dla API panelu administracyjnego

>>>>>>> e31bb3a7db81f9e4d1a1ace85d4547898d31b732

class CompanySerializer(ModelSerializer):
    name = CharField(required=True, label=_("names"), help_text=_(
        "write_company names"))

    class Meta:
        model = Company
        fields = '__all__'

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu User istotnych dla API panelu administracyjnego


class UserSerializer(ModelSerializer):

    full_name = SerializerMethodField(source='get_full_name')
<<<<<<< HEAD

    class Meta:
        model = User
        fields = ['id','first_name','last_name','full_name','email','id_card','company']
        
=======
    company = CompanySerializer(read_only=True)
    id_card = CharField(required=True, label=_("id_card"), help_text=_(
        "write ID Card"))

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name',
                  'full_name', 'email', 'id_card', 'company']
        depth = 0
        lookup_field = 'card_slug'
        extra_kwargs = {
            'url': {'lookup_field': 'card_slug'}
        }

>>>>>>> e31bb3a7db81f9e4d1a1ace85d4547898d31b732
    def get_full_name(self, obj):
        return f'{obj.first_name} {obj.last_name}'

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu Answer istotnych dla API panelu administracyjnego


class AnswerSerializer(ModelSerializer):
    answer = CharField(required=True, label=_(
        "answers"), help_text=_("write answer"))

    class Meta:
        model = Answer
        fields = ['id', 'answer']

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu Question istotnych dla API panelu administracyjnego


class QuestionSerializer(ModelSerializer):
    content = CharField(required=True, label=_("content"), help_text=_(
        "write question content"))
    answers = AnswerSerializer(many=True, partial=True, read_only=True)
    sequence = IntegerField(label=_("sequence"), help_text=_(
        "set sequence question"), validators=[validate_minus_number])

    class Meta:
        model = Question
        fields = ['id', 'content','sequence','language','answers','image']
        depth = 1

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu Training istotnych dla API panelu administracyjnego


class TrainingSerializer(ModelSerializer):
    name = CharField(required=True, label=_("names"), help_text=_(
        "write_name_trainings"))
    users = UserSerializer(many=True, read_only=True, partial=True)
    question = QuestionSerializer(many=True, read_only=True, partial=True)
    time = IntegerField(required=True, label=_("Time"), help_text=_(
        "Write time"), validators=[validate_training_days, validate_minus_number])

    class Meta:
        model = Training
        fields = '__all__'

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu CompletedTraining istotnych dla API panelu administracyjnego


class CompletedTraningSerializer(ModelSerializer):

    expiration_date = SerializerMethodField()
    class Meta:
        model = CompletedTraining
        fields = ["id", "guest", "training",
                  "completed_date", "expiration_date"]

    def get_expiration_date(self, obj):
        return obj.expiration_date

# Definicja serializera odpowiadającego za nasłuchiwanie parametrów z modelu QuestionsAnswersSet istotnych dla API panelu administracyjnego


class QuestionsAnswersSetSerializer(ModelSerializer):


    class Meta:
        model = QuestionsAnswersSet
        fields = ['id', 'question', 'answer', 'status']

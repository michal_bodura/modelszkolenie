from .admin_actions import *
from .admin_inlines import *
from .filters import *
from .forms import *
from .models import *
from .paginators import *
from .resources import *
from .widgets import *
from daterangefilter.filters import DateRangeFilter
from django.contrib.admin import HORIZONTAL, ModelAdmin
from django.utils.translation import gettext_lazy as _
from import_export.admin import ExportMixin, ImportExportMixin, ImportExportModelAdmin


class QuestionAdminFields(ImportExportModelAdmin, ModelAdmin):
    inlines = [
        QuestionAnswersSetInline
    ]
    actions = [AdminActions.set_english, AdminActions.set_polish]
    show_full_result_count = True
    paginator = QuestionPaginator
    action_callback = True
    resource_class = QuestionResource
    autocomplete_fields = ('image',)
    list_display = ['content', 'numOfAnswers', 'language', "sequence"]
    list_filter = [NumAnswersFilter, 'language']
    search_fields = ['content', 'language', 'answers__answer']
    list_display_links = ['content']
    list_per_page = 20
    radio_fields = {"language": HORIZONTAL}
    fieldsets = [
        (_('Question content'), {'fields': [
         'content', 'language', 'image', 'sequence']}),
    ]


class TrainingAdminFields(ImportExportModelAdmin, ModelAdmin):
    inlines = [
        GaleryImageTrainingSetInline,
        QuestionTrainingSetInline,
    ]
    actions = [AdminActions.mark_obligatory, AdminActions.mark_unobligatory,
               AdminActions.set_polish, AdminActions.set_english]
    show_full_result_count = True
    paginator = TrainingPaginator
    action_callback = True
    autocomplete_fields = ('image',)
    resource_class = TrainingResource
    list_filter = ['language', TrainingObligatoryFilter,
                   TrainingTimeFilter, NumQuestionsFilter, NumGuestsFilter]
    list_display = ['name', 'time_str', 'obligatory',
                    'numOfGuests', 'numOfQuestions', 'numOfPictures', 'language']
    search_fields = ['name', 'time', 'language']
    list_display_links = ['name']
    list_per_page = 20
    radio_fields = {"language": HORIZONTAL}
    fieldsets = [
        (_('Training data'), {'fields': [
         'name', 'time', 'obligatory', 'image', 'language']}),
    ]


class AnswerAdminFields(ImportExportModelAdmin, ModelAdmin):
    actions = [AdminActions.set_english, AdminActions.set_polish]
    show_full_result_count = True
    paginator = AnswerPaginator
    action_callback = True
    resource_class = AnswerResource
    fields = ['answer', 'language']
    list_display = ['answer', 'language']
    list_per_page = 20
    search_fields = ['answer', 'language']
    radio_fields = {"language": HORIZONTAL}


class CompanyAdminFields(ImportExportMixin, ModelAdmin):
    show_full_result_count = True
    paginator = CompanyPaginator
    action_callback = True
    resource_class = CompanyResource
    fieldsets = [
        (_('Company data'), {'fields': ['name', 'address', 'city']})
    ]
    list_display = ['name', 'real_address']
    search_fields = ['name', 'address', 'city']
    list_display_links = ['name']
    list_filter = [CityFilter]
    list_per_page = 20


class GuestAdminFields(ImportExportModelAdmin, ModelAdmin):
    show_full_result_count = True
    paginator = GuestPaginator
    action_callback = True
    resource_class = UserResource
    add_form = [CustomUserCreationForm]
    form = CustomUserChangeForm
    list_display = ['first_name', 'last_name',
                    'email', 'id_card', 'country', 'company']
    autocomplete_fields = ('company',)
    list_display_links = ['email', 'id_card']
    list_filter = ['company__name']
    search_fields = ['email', 'id_card', 'country',
                     'company__name', 'company__city']
    list_per_page = 25

    fieldsets = [
        (_('User data'), {'fields': [
         ('first_name', 'last_name'), ('email', 'id_card')]}),
        (_('Additional data'), {'fields': ['company', 'country']}),
        (_('Data'), {'classes': ('collapse', 'open'), 'fields': ['card_slug']})
    ]
    prepopulated_fields = {"card_slug": ("id_card",)}


class CompletedTrainingAdminFields(ExportMixin, ModelAdmin):
    formfield_overrides = {
        DateField: {'widget': CustomDatePickerWidget}
    }
    show_full_result_count = True
    paginator = CompletedTrainingPaginator
    action_callback = True
    date_hierarchy = 'completed_date'
    date_hierarchy_drilldown = False
    resource_class = CompletedTrainingResource
    readonly_fields = ["expiration_date"]
    autocomplete_fields = ('guest', 'training')
    list_display = ['guest', 'expiration_date_colored',
                    'completed_date', 'training']
    list_display_links = ['guest']
    list_filter = [['completed_date', DateRangeFilter], ExpDateFilter,
                   'guest__company__name', 'guest__id_card']
    list_per_page = 20

    search_fields = ['guest__email', 'guest__id_card',
                     'guest__company__name', 'training__name']
    fieldsets = [
        (_('Personlal date'), {'fields': ['guest', 'training']}),
        (_('my completed training'), {'classes': ('collapse', 'open'), 'fields': [
         'completed_date', 'expiration_date']}),

    ]


class QuestionsAnswersSetAdminFields(ExportMixin, ModelAdmin):
    actions = [AdminActions.mark_correct, AdminActions.mark_uncorrect]
    show_full_result_count = True
    paginator = QuestionsAnswersSetPaginator
    action_callback = True
    resource_class = QuestionsAnswersSetResource
    list_display = ['question', 'answer', 'status']
    list_display_links = ['question', 'answer']
    list_filter = [QuestionsAnswersSetFilter, 'question']
    list_per_page = 20
    search_fields = ['question__content', 'answer__answer', 'status']
    fieldsets = [
        (_('Question state'), {'fields': ['question', 'answer', 'status']}),
    ]


class TrainingImageAdminFields(ExportMixin, ModelAdmin):
    show_full_result_count = True
    paginator = TrainingImagePaginator
    action_callback = True
    resource_class = TrainingImageResource
    date_hierarchy = 'date'
    date_hierarchy_drilldown = False
    list_filter = [['date', DateRangeFilter]]
    list_display = ["titleImage", "thumbnail", "date"]
    list_display_links = ['titleImage']
    list_per_page = 20
    search_fields = ['titleImage', 'date']

    fieldsets = [
        (None, {'fields': ['titleImage']}),
        (_('Image data'), {'fields': ["image"]}),
    ]


class QuestionImageAdminFields(ExportMixin, ModelAdmin):
    show_full_result_count = True
    paginator = QuestionImagePaginator
    action_callback = True
    resource_class = QuestionImageResource
    date_hierarchy = 'date'
    date_hierarchy_drilldown = True
    list_display = ["titleImage", "thumbnail", "date"]
    list_filter = [['date', DateRangeFilter]]
    list_display_links = ['titleImage']
    search_fields = ['titleImage', 'date']
    list_per_page = 20

    fieldsets = [
        (None, {'fields': ['titleImage']}),
        (_('Image data'), {'fields': ["image"]}),
    ]


class GaleryImageAdminFields(ExportMixin, ModelAdmin):
    show_full_result_count = True
    paginator = GaleryImagePaginator
    action_callback = True
    list_display = ["name", "thumbnail", "sequence"]
    list_filter = ['sequence']
    search_fields = ['name', ['date', DateRangeFilter]]
    list_per_page = 20
    fieldsets = [
        (None, {'fields': ['name', 'sequence']}),
        (_('Image data'), {'fields': ["image"]}),
    ]

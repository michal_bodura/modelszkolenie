from django.db.models import Manager, F, DateField, ExpressionWrapper
from django.db.models.functions import TruncDate

class CompletedTrainingManager(Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(expiration_date=ExpressionWrapper(TruncDate(F('completed_date')) + F('training__time'), output_field=DateField()))

from .models import *
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

# Add model signals for media files

@receiver(pre_delete, sender=TrainingImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)

@receiver(pre_delete, sender=GaleryImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)

@receiver(pre_delete, sender=QuestionImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)
from django.core.paginator import Paginator
from django.utils.functional import cached_property
from django.utils.inspect import method_has_no_args
from .models import *
import inspect
MAX_RECORDS = 50000


class GuestPaginator(Paginator):
    allGuests = User.objects.all().count()

    @cached_property
    def count(self):
        # Kod oparty na klasie źródłowej Paginator
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allGuests < MAX_RECORDS else MAX_RECORDS


class CompanyPaginator(Paginator):
    allCompanies = Company.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allCompanies < MAX_RECORDS else MAX_RECORDS


class TrainingPaginator(Paginator):
    allTrainings = Training.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allTrainings < MAX_RECORDS else MAX_RECORDS


class TrainingImagePaginator(Paginator):
    allTrainingImages = TrainingImage.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allTrainingImages < MAX_RECORDS else MAX_RECORDS


class QuestionImagePaginator(Paginator):
    allQuestionImages = QuestionImage.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allQuestionImages < MAX_RECORDS else MAX_RECORDS


class GaleryImagePaginator(Paginator):
    allGaleryImages = GaleryImage.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allGaleryImages < MAX_RECORDS else MAX_RECORDS


class QuestionsAnswersSetPaginator(Paginator):
    allQuesitonsAnswersSets = QuestionsAnswersSet.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allQuesitonsAnswersSets < MAX_RECORDS else MAX_RECORDS


class CompletedTrainingPaginator(Paginator):
    allCompletedTrainings = CompletedTraining.unexpirated.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allCompletedTrainings < MAX_RECORDS else MAX_RECORDS


class QuestionPaginator(Paginator):
    allQuestions = Question.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allQuestions < MAX_RECORDS else MAX_RECORDS


class AnswerPaginator(Paginator):
    allAnswers = Answer.objects.all().count()

    @cached_property
    def count(self):
        c = getattr(self.object_list, 'count', None)
        if callable(c) and not inspect.isbuiltin(c) and method_has_no_args(c):
            return c()
        return len(self.object_list) if self.allAnswers < MAX_RECORDS else MAX_RECORDS

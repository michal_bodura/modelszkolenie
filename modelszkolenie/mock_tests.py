from datetime import date
from unittest.mock import patch


from django.test import TestCase


from .functions import myfunc_using_date


def mocked_today():
    return date(year=2020, month=1, day=1)

class TestImmutableObject(TestCase):
    @patch("modelszkolenie.functions.get_today", mocked_today)
    def test_myfunc_using_date(self):
        self.assertEqual(myfunc_using_date().strftime(
            "%Y-%m-%d"), "2020-01-01")

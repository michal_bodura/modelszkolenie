from .models import *
from django.contrib.admin import TabularInline
from django.utils.translation import gettext_lazy as _


class QuestionAnswersSetInline(TabularInline):
    model = Question.answers.through
    verbose_name = _("Q&A set")
    extra = 1
    min_num = 1
    max_num = 5
    classes = ['collapse', 'show']


class QuestionTrainingSetInline(TabularInline):
    model = TrainingQuestionProxy
    verbose_name = _("QuestionSet")
    verbose_name_plural = _("List of questions")
    extra = 1
    classes = ['collapse', 'show']


class GaleryImageTrainingSetInline(TabularInline):
    model = TrainingGalleryProxy
    verbose_name = _("Gallery images")
    verbose_name_plural = _("Gallery")
    extra = 1
    classes = ['collapse', 'show']

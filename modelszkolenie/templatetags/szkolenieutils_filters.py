from django import template
from django.utils.html import format_html
from django.utils.encoding import force_text

register = template.Library()

@register.filter()
def to_accusative_verbose_name(value, model):
    verbose_name = model._meta.verbose_name  # declared in Meta
    new_name = force_text(model.accusative_case_verbose_name())  # a custom class method (lives in your Model)
    return format_html(value.replace(str(verbose_name), new_name))


@register.filter()
def modulo(num, val):
    return num % val
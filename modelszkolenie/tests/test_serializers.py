import pytest
from modelszkolenie.serializers import *
from modelszkolenie.models import *

@pytest.mark.django_db
class TestUsererializer:

    @pytest.fixture
    def setup_method(self, db, tool_factory):
        self.tool_attributes = {
            'first_name': "Robert",
            'last_name': "Koraczyk",
            'id_card': "KUO94315",
        }

        self.serializer_data = {
            'first_name': "Robert",
            'last_name': "Koraczyk",
            'id_card': "KUO94315",
        }
        self.tool = tool_factory(**self.tool_attributes)
        self.serializer = UserSerializer(instance=self.tool)
        assert self.tool_attributes == self.serializer_data
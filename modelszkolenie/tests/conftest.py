# conftest.py
from rest_framework.test import APIClient
import pytest

@pytest.fixture
def api_client():
    return APIClient

def pytest_collection_modifyitems(items):
    for item in items:
        item.add_marker('all')
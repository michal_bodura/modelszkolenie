from django.urls import reverse, resolve

class TestUrls:
    
    def test_currentUser_url(self):
        path = reverse('currentUser', kwargs={'pk':9}) 
        assert resolve(path).view_name == "currentUser"
    
    def test_companyDetail_url(self):
        path = reverse('currentCompany', kwargs={'pk':5}) 
        assert resolve(path).view_name == "currentCompany"
    
    def test_completedTraining_url(self):
        path = reverse('completedTraining', kwargs={'pk':5}) 
        assert resolve(path).view_name == "completedTraining"
    
    def test_currentQuestion_url(self):
        path = reverse('currentQuestion', kwargs={'pk':5}) 
        assert resolve(path).view_name == "currentQuestion"
    
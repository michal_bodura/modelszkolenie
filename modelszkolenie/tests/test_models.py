from modelszkolenie.models import CompletedTraining, User
from mixer.backend.django import mixer
from datetime import date, timedelta
import pytest
import uuid

@pytest.fixture
def test_password():
   return 'malutki12'

@pytest.fixture
def create_user(db, django_user_model, test_password):
   def make_user(**kwargs):
       kwargs['password'] = test_password
       if 'username' not in kwargs:
           kwargs['username'] = str(uuid.uuid4())
       return django_user_model.objects.create_user(**kwargs)
   return make_user

@pytest.fixture
def auto_login_user(db, client, create_user, test_password):
   def make_auto_login(user=None):
       if user is None:
           user = create_user()
       client.login(username=user.username, password=test_password)
       return client, user
   return make_auto_login

@pytest.mark.django_db
class TestUserModels:
    # Sprawdzanie użytkownika w oparciu o imię i nazwisko
    def test_user_in_training(self):
        user = mixer.blend('modelszkolenie.User', first_name='Michał',last_name="Bodura", id_card='CDJ924104')
        assert user.first_name == 'Michał' 
    # Sprawdzanie użytkownika w oparciu o numer dowodu osobistego
    def test_user_in_training(self):
        user = mixer.blend('modelszkolenie.User', first_name='Michał',last_name="Nowak", id_card='OID28521')
        assert user.id_card == 'OID28521'
    
    def test_fk_training(self):
        user = mixer.blend(User, first_name='Michał',last_name="Nowak", id_card='OID28521',company__name='IU Technology')
        assert user.company.name == 'IU Technology'
    
    def test_user_create(self):
        custom_user = User.objects.create(
            first_name="John",
            last_name="Doe",
            email="john@gmail.com",
            id_card="KAK9432545",
        )
        assert custom_user.email == "john@gmail.com"
        assert custom_user.id_card == "KAK9432545"

@pytest.mark.django_db
class TestTrainingModels:
        # Sprawdzanie użytkownika w oparciu o przynależność do firmy
    def test_training(self):
        user = mixer.blend('modelszkolenie.Training', name='Szkolenie BHP',time=12)
        assert user.time > 10
    
    def test_training(self):
        user = mixer.blend('modelszkolenie.Training', name='Szkolenie HR',time=1)
        assert user.time != 0

    # Wpisanie wartości stringowej - wywala komunikacja wartością coś tam musi być liczbą całkowitą
    # def test_training(self):
    #     user = mixer.blend('modelszkolenie.Training', name='Szkolenie HR',time="goig")
    #     assert type(user.time) == 'str'

@pytest.mark.django_db
class TestCompletedTraining:
        # Sprawdzanie użytkownika w oparciu o przynależność do firmy
    def test_completed_training(self):
        completed_training = mixer.blend(CompletedTraining, guest__id_card="KO932145",completed_date=date(2021,9,14), training__time=14)
        assert completed_training.guest.id_card == "KO932145"
        assert completed_training.completed_date < date(2021,9,14) + timedelta(days=completed_training.training.time)

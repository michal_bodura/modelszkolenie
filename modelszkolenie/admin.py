
# Import bibliotek z tego samego katalogu
from .admin_fields import *
from .admin_actions import *
from .filters import *
from .forms import *
from .models import *
from .paginators import *
from .resources import *
from datetime import timedelta
# Import komponentów istotnych dla panelu administracyjnego

from django.contrib.admin import AdminSite, display as Display, sites as Sites
from django.contrib.messages import success, error

# Import komponentu odpowiadającego za możliwość tłumaczenia elementów na język polski z możliwością
# nadania formy liczbowej

from django.utils.translation import gettext_lazy as _
from django.utils.html import format_html
# Import biblioteki django-admin-easy odpowiadającej za dodawanie opisów w formie dekoratorów

from easy import short as Description

import babel.dates

class MyAdminSite(AdminSite):
    index_title = _('Welcome')
    site_header = _('Admin panel')
    site_title = _('Admin panel')
    empty_value_display = "---"

    def get_app_list(self, request):
        """
        Return a sorted list of all the installed apps that have been
        registered in this site.
        """
        ordering = {
            "Goście": 1,
            "Firmy": 2,
            "Szkolenia": 3,
            "Pytania": 4,
            "Odpowiedzi": 5,
            "Ukończone szkolenia": 6,
            "Galerie obrazów": 7,
            "Obrazki do pytania": 8,
            "Obrazki do szkolenia": 9,
            "Użytkownicy": 10,
            "Autoryzowane grupy": 11,
            "Szablony": 12,
            "Próby zalogowania": 13
        }
        app_dict = self._build_app_dict(request)

        app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())

        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: ordering[x['name']])

        return app_list


# Inicjacja klasy MyAdminSite z widoku panelu administracyjnego

my_admin_site = MyAdminSite(name='admin')

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Question


class QuestionAdmin(QuestionAdminFields, AdminActions):

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(Count('answers'))
        return qs

    @Description(short_description=_('NumOfAnswers'), admin_order_field="answers__count")
    @Display(empty_value="---")
    def numOfAnswers(self, obj: Question) -> str:
        return obj.numOfAnswers if obj.numOfAnswers > 0 else _("Not")

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionsFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.content == form.cleaned_data['content']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed question sucessfully"))
                else:
                    error(request, _("Changed question unsucessfully"))
            else:
                if obj.content == form.cleaned_data['content']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added question sucessfully"))
                else:
                    error(request, _("Added question unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted question sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            super().delete_queryset(request, queryset)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='question was deleted sucessfully.', text2='questions were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='question was deleted sucessfully.', text2='questions pro were deleted sucessfully.')


# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Training


class TrainingAdmin(TrainingAdminFields):

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(Count('users'))
        qs = qs.annotate(Count('questions'))
        return qs

    @Description(short_description=_('ActiveDuration'), admin_order_field='time')
    @Display(empty_value="---")
    def time_str(self, obj: Training) -> str:
        return obj.time_str

    @Description(short_description=_('NumOfGuests'), admin_order_field="users__count")
    @Display(empty_value="---")
    def numOfGuests(self, obj: Training) -> str:
        return obj.numOfGuests if obj.numOfGuests > 0 else _("Not")

    @Description(short_description=_('NumOfPictures'), admin_order_field="gallery__count")
    @Display(empty_value="---")
    def numOfPictures(self, obj: Training) -> str:
        return obj.numOfPictures if obj.numOfPictures > 0 else _("Not")

    @Description(short_description=_('NumOfQuestions'), admin_order_field="questions__count")
    @Display(empty_value="---")
    def numOfQuestions(self, obj: Training) -> str:
        return obj.numOfQuestions if obj.numOfQuestions > 0 else _("Not")

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("TrainingsFile"),
                              file_format.get_extension())
        return filename

    def get_ordering(self, request):
        return ['-time']

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed training sucessfully"))
                else:
                    error(request, _("Changed training unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added training sucessfully"))
                else:
                    error(request, _("Added training unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted training sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            super().delete_queryset(request, queryset)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='training was deleted sucessfully.', text2='trainings were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='training was deleted sucessfully.', text2='trainings pro were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Answer


class AnswerAdmin(AnswerAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("AnswersFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.answer == form.cleaned_data['answer']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed answer sucessfully"))
                else:
                    error(request, _("Changed answer unsucessfully"))
            else:
                if obj.answer == form.cleaned_data['answer']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added answer sucessfully"))
                else:
                    error(request, _("Added answer unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted answer sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            AdminActions.success_delete_custom_message_user(request, record_num,
                                                            text1='answer was deleted sucessfully.', text2='answers were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Company


class CompanyAdmin(CompanyAdminFields):

    @Description(short_description=_('real_address'), admin_order_field='address')
    @Display(empty_value="---")
    def real_address(self, obj: Company) -> str:
        return obj.real_address if (obj.address and obj.city) \
            else f"{obj.address} - " + _("Street unknown") if obj.address \
            else f"{obj.city} - " + _("Street unknown") if obj.city \
            else _("Unknown")

    def get_ordering(self, request):
        return ['name']

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("CompaniesFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed company sucessfully"))
                else:
                    error(request, _("Changed company unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added company sucessfully"))
                else:
                    error(request, _("Added company unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted company sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='company was deleted sucessfully.', text2='companies were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='company was deleted sucessfully.', text2='companies pro were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu User


class GuestAdmin(GuestAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("GuestsFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.id_card == form.cleaned_data['id_card']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed guest sucessfully"))
                else:
                    error(request, _("Changed guest unsucessfully"))
            else:
                if obj.id_card == form.cleaned_data['id_card']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added guest sucessfully"))
                else:
                    error(request, _("Added guest unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted guest sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            AdminActions.success_delete_custom_message_user(request, record_num,
                                                            text1='guest was deleted sucessfully.', text2='guests were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu CompletedTraining


class CompletedTrainingAdmin(CompletedTrainingAdminFields):

   
    def queryset(self, request):
        return super(CompletedTraining, self).queryset(request).select_related('expiration_date')
    @Description(short_description=_('Expiration date'), help_text=_('Expiration date'), admin_order_field=F("completed_date")+F("training__time"))
    @Display(empty_value="---")
    def expiration_date(self, obj: CompletedTraining) -> str:
        return obj.expiration_date

    @Description(short_description=_('Expiration date'), help_text=_('Expiration date'), admin_order_field=F("completed_date")+F("training__time"))
    def expiration_date_colored(self, obj):
        if obj._expiration_date < date.today():
            color_code = '990000'
        elif obj._expiration_date >= date.today() + timedelta(days=2):
            color_code = '009900'
        else:
            color_code = '999900'
        exp_date = babel.dates.format_date(obj._expiration_date, 'd MMMM yyyy', locale='pl_PL')
        html = '<b><span style="color: #{};">{}</span></b>'.format(color_code,exp_date)
        return format_html(html)

    def get_ordering(self, request):
        return ['-completed_date']

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("CompletedTrainingsFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.guest == form.cleaned_data['guest']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed completed training sucessfully"))
                else:
                    error(request, _("Changed completed training unsucessfully"))
            else:
                if obj.guest == form.cleaned_data['guest']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added completed training sucessfully"))
                else:
                    error(request, _("Added completed training unsucessfully"))

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        success(request, _("Deleted completed training sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='completed training was deleted sucessfully.', text2='completed trainings were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='completed training was deleted sucessfully.', text2='completed trainings pro were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu QuestionsAnswersSet


class QuestionsAnswersSetAdmin(QuestionsAnswersSetAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionsAnswersSetFile"),
                              file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.question == form.cleaned_data['question']:
                    super().save_model(request, obj, form, change)
                    success(request, _(
                        "Changed questions and answers sucessfully"))
                else:
                    error(request, _("Changed questions and answers unsucessfully"))
            else:
                if obj.question == form.cleaned_data['question']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added questions and answers sucessfully"))
                else:
                    error(request, _("Added questions and answers unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted questions and answers set sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='questions and answers set was deleted sucessfully.', text2='questions and answers sets were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='questions and answers set deleted sucessfully.', text2='questions and answers sets pro were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu TrainingImage


class TrainingImageAdmin(TrainingImageAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("TrainingImagesFile"),
                              file_format.get_extension())
        return filename
    @Description(short_description=_('Thumbnail'))
    def thumbnail(self, obj):
        width, height = 100, 100
        html = '<img src="{url}" width="{width}" height={height} />'
        return format_html(html.format(url=obj.image.url, width=width, height=height))

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed training image sucessfully"))
                else:
                    error(request, _("Changed training image unsucessfully"))
            else:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added training image sucessfully"))
                else:
                    error(request, _("Added training image unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted training image sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='training image was deleted sucessfully.', text2='training images were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='training image deleted sucessfully.', text2='training images were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu QuestionImage


class QuestionImageAdmin(QuestionImageAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionImagesFile"),
                              file_format.get_extension())
        return filename
    @Description(short_description=_('Thumbnail'))
    def thumbnail(self, obj):
        width, height = 100, 100
        html = '<img src="{url}" width="{width}" height={height} />'
        return format_html(html.format(url=obj.image.url, width=width, height=height))
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed question image sucessfully"))
                else:
                    error(request, _("Changed question image unsucessfully"))
            else:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added question image sucessfully"))
                else:
                    error(request, _("Added question image unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted question image sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='question image was deleted sucessfully.', text2='question images were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='question image deleted sucessfully.', text2='question images pro were deleted sucessfully.')

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu GaleryImage


class GaleryImageAdmin(GaleryImageAdminFields):

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("GalleryImage"),
                              file_format.get_extension())
        return filename
    @Description(short_description=_('Thumbnail'))
    def thumbnail(self, obj):
        width, height = 100, 100
        html = '<img src="{url}" width="{width}" height={height} />'
        return format_html(html.format(url=obj.image.url, width=width, height=height))
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        success
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed galery image sucessfully"))
                else:
                    error(request, _("Changed galery image unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added galery image sucessfully"))
                else:
                    error(request, _("Added galery image unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted galery image sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request, queryset)
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='gallery image was deleted sucessfully.', text2='gallery images were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='gallery image was deleted sucessfully.', text2='gallery images pro were deleted sucessfully.')

# Wyświetlanie danych zdefiniowanych w modelu w panelu administracyjnym

custom_modelsAdmins = [[User, Company, Training, Question, TrainingImage, GaleryImage, QuestionImage, CompletedTraining, Answer],
            [GuestAdmin, CompanyAdmin, TrainingAdmin, QuestionAdmin, TrainingImageAdmin, GaleryImageAdmin, QuestionImageAdmin, CompletedTrainingAdmin, AnswerAdmin]]

for i in range(len(custom_modelsAdmins[0])):
    try:
        my_admin_site.register(custom_modelsAdmins[0][i], custom_modelsAdmins[1][i])
    except Sites.AlreadyRegistered:
        pass

from django.contrib.auth.admin import UserAdmin as AuthUserAdmin, GroupAdmin as AuthGroupAdmin
from admin_interface.admin import ThemeAdmin

class CustomGroupAdmin(AuthGroupAdmin):

    list_display = ["name"]
    list_per_page = 20

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed auth group sucessfully"))
                else:
                    error(request, _("Changed auth group unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added auth group sucessfully"))
                else:
                    error(request, _("Added auth group unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted auth group sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            super().delete_queryset(request, queryset)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='auth group was deleted sucessfully.', text2='auth groups were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='auth group was deleted sucessfully.', text2='auth groups pro were deleted sucessfully.')

    class Meta:
        model = CustomGroup


class CustomUserAdmin(AuthUserAdmin):
    list_per_page = 20
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_display = ["username", "email",
                    "first_name", "last_name", "get_groups"]

    @Description(short_description=_('PermissionGroups'))
    def get_groups(self, obj):
        return "\n".join([p.name for p in obj.groups.all()])

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.username == form.cleaned_data['username']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed auth user sucessfully"))
                else:
                    error(request, _("Changed auth user unsucessfully"))
            else:
                if obj.username == form.cleaned_data['username']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added auth user sucessfully"))
                else:
                    error(request, _("Added auth user unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted auth user sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            super().delete_queryset(request, queryset)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='auth user was deleted sucessfully.', text2='auth users were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='auth user was deleted sucessfully.', text2='auth users pro were deleted sucessfully.')

    class Meta:
        model = CustomUser


class CustomAdminInterfaceTheme(ThemeAdmin):
    list_per_page = 20

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request, args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                super().save_model(request, obj, form, change)
                success(request, _("Changed admin interface sucessfully"))
            else:
                super().save_model(request, obj, form, change)
                success(request, _("Added admin interface sucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted admin interface sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            polish_plural_expr = AdminActions.get_polish_plural_expr(
                record_num)
            super().delete_queryset(request, queryset)
            if (polish_plural_expr):
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='admin interface was deleted sucessfully.', text2='admin interfaces were deleted sucessfully.')
            else:
                AdminActions.success_delete_custom_message_user(request, record_num,
                                                                text1='admin interface was deleted sucessfully.', text2='admin interfaces pro were deleted sucessfully.')

    class Meta:
        model = CustomInterfaceTheme

custom_authAdmins = [[CustomGroup, CustomUser, CustomInterfaceTheme], 
[CustomGroupAdmin, CustomUserAdmin, CustomAdminInterfaceTheme]]

for i in range(len(custom_authAdmins[0])):
    try:
        my_admin_site.register(custom_authAdmins[0][i], custom_authAdmins[1][i])
    except Sites.AlreadyRegistered:
        pass

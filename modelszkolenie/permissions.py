from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import BasePermission

class IsSuperUser(BasePermission):

    message = _('You are not superuser')

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class IsAdministrator(BasePermission):

    message = _('You are not allowed here')

    def has_permission(self, request, view):
        return bool(request.user and request.user.groups.filter(name__in=['Administrator', 'Superadministrator']).exists())


from .models import *
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ChoiceField, Select
from django.utils.translation import gettext_lazy as _
# Klasa definiująca sposób tworzenia danych opisujących kraj, z którego pochodzi gość w polu country w modelu User


class CustomUserCreationForm(UserCreationForm):

    country = ChoiceField(
        choices=[('pl', 'Polska'), ('gb', 'Wielka Brytania')],
        label=_("Country"),
        widget=Select(
            attrs={
                'class': 'form-control m-b',
                'id': 'country_sel'
            }
        )
    )

    class Meta(UserCreationForm):
        model = User
        fields = '__all__'

# Klasa definiująca sposób edycji danych opisujących kraj, z którego pochodzi gość w polu country w modelu User


class CustomUserChangeForm(UserChangeForm):

    country = ChoiceField(
        choices=[('pl', 'Polska'), ('gb', 'Wielka Brytania')],
        label=_("Country"),
        widget=Select(
            attrs={
                'class': 'form-control m-b',
                'id': 'country_sel'
            }
        )
    )

    class Meta(UserChangeForm):
        model = User
        fields = '__all__'

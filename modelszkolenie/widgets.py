from datetime import date
from django.forms import DateInput

class CustomDatePickerWidget(DateInput):

    DATE_INPUT_WIDGET_REQUIRED_FORMAT = '%Y-%m-%d'

    def __init__(self,  attrs={}, format='%Y-%m-%d'):
        
        attrs.update(
            {
                'class':'form-control',
                'type':'date',
            }
        )
        self.format = format or self.DATE_INPUT_WIDGET_REQUIRED_FORMAT
        super().__init__(attrs, format=self.format)


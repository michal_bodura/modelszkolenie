from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from os import path as Path


# Walidacja rozszerzenia wczytanego pliku


def validate_file_extension(value):
    ext = Path.splitext(value.name)[1]
    print(ext)
    print(value)

    valid_extensions = ['.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Choose image only in .jpg or .png'))


def validate_url_extension(value):
    remoteFile = value.split('/')[-1]
    ext = Path.splitext(remoteFile)[1]
    valid_extensions = ['.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Choose image only in .jpg or .png'))


def validate_training_days(value):
    if value == 0:
        raise ValidationError(
            _('%(value)s is not valid - training must have been at least 1 day'),
            params={'value': value},
        )


def validate_minus_number(value):
    if value < 0:
        raise ValidationError(
            _('%(value)s is not valid - value must not have a negative value'),
            params={'value': value},
        )


def file_size(value):
    limit = 20 * 1024 * 1024
    if value.size > limit:
        raise ValidationError(
            _('File too large. Size should not exceed 20 MiB.'))

from .managers import *
from .validators import *

from admin_interface.models import Theme
from datetime import datetime, timedelta
from django_countries.fields import CountryField
from django.conf import settings
from django.contrib.auth.models import Group as AuthGroup, User as AuthUser, AbstractBaseUser, UserManager
from django.db.models import CharField, PositiveIntegerField, FileField, BooleanField, SlugField, DateTimeField, TextField, EmailField, ManyToManyField, F, Count, Model, TextChoices, ForeignKey, DateField, CASCADE
from django.db.models.signals import pre_delete, post_save
from django.dispatch.dispatcher import receiver
from django.utils.translation import gettext_lazy as _


# Możliwość definiowania paramterów w języku polskim

from locale import setlocale as SetLanguage, LC_ALL as ALL
import os
import time
SetLanguage(ALL, '')


class CustomInterfaceTheme(Theme):

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Custom Interface acc'))

    class Meta:
        app_label = 'admin_interface'
        verbose_name_plural = _("Custom themes")
        verbose_name = _("Custom theme")


class CustomGroup(AuthGroup):

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Custom Group acc'))

    class Meta:
        app_label = 'auth'
        verbose_name_plural = _("Auth Group plural")
        verbose_name = _("Auth Group")


class CustomAbstractUser(AbstractBaseUser):
    ...
    objects = UserManager()


class CustomUser(AuthUser):

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Custom User acc'))

    class Meta:
        app_label = 'auth'
        verbose_name_plural = _("Auth user plural")
        verbose_name = _("Auth user")


class Company(Model):

    name = CharField(max_length=60, help_text=_(
        "write company name"), verbose_name=_("name"), blank=False, null=True)
    address = CharField(max_length=120, blank=True, null=True, help_text=_(
        "write address"), verbose_name=_("address"))
    city = CharField(max_length=180, blank=True, null=True,
                     help_text=_("write city"), verbose_name=_("city"))

    def __str__(self):
        return \
            f"{self.name} - {self.city} {self.address}" \
            if (self.city and self.address) \
            else f"{self.name} - {self.city}" if self.city\
            else f"{self.name} - {self.address}" if self.address\
            else f"{self.name}"

    @property
    def real_address(self):
        return f"{self.address}, {self.city}"

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Companies acc'))

    @classmethod
    def accusative_case_verbose_name_plural(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Companies pro acc'))

    class Meta:

        verbose_name_plural = _("company plural")
        verbose_name = _("companyOdm")
        unique_together = ['name', 'address']
        db_table = "Company"
        ordering = ['name']


# Definicja modelu wyliczającego Language w postaci klasy


class Language(TextChoices):

    ENGLISH = 'EN', _('English')
    POLISH = 'PL', _('Polish')

# Definicja modelu User w postaci klasy


class User(Model):

    first_name = CharField(max_length=30, verbose_name=_(
        "first name"), help_text=_("write first name"), null=True, blank=True)
    last_name = CharField(max_length=30, verbose_name=_(
        "last name"), help_text=_("write last name"), null=True, blank=True)
    email = EmailField(help_text=_("write email"), verbose_name=_(
        "email"), unique=True, null=True, blank=True)
    id_card = CharField(max_length=26, verbose_name=_("id_card"), help_text=_(
        "write ID Card"), blank=False, unique=True, null=True)
    country = CountryField(blank_label=_("Choose country"), null=True, blank=True, verbose_name=_(
        "Country"), help_text=_("Choose country"), default='PL')

    card_slug = SlugField(max_length=200,  blank=False, null=True)
    company = ForeignKey(
        Company,
        related_name='companies',
        on_delete=CASCADE,
        blank=True,
        null=True,
        verbose_name=_("Company"),
        help_text=_("Choose company")

    )

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('User acc'))

    def __str__(self):
        return \
            f"{self.first_name} - {self.id_card}" if self.first_name \
            else f"{self.email} - {self.id_card}" if self.email \
            else f"{self.last_name} - {self.id_card}" if self.last_name \
            else f"{self.first_name} {self.last_name} - {self.id_card}" \
            if (self.first_name and self.last_name) \
            else f"{self.id_card}"

    class Meta:
        unique_together = ['email', 'id_card']
        verbose_name_plural = _("Guest plural")
        verbose_name = _("GuestOdm")
        db_table = "Guest"

# Definicja modelu Answer w postaci klasy


class Answer(Model):

    answer = TextField(verbose_name=_("answer"), help_text=_(
        "write answer"), blank=False, null=True)
    language = CharField(
        max_length=2,
        choices=Language.choices,
        default='PL',
        verbose_name=_("Language"),
        help_text=_("Choose language")
    )

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Answer acc'))

    def __str__(self):
        return f"{self.answer}, Język: {self.language}"

    class Meta:
        ordering = ['id']
        verbose_name = _("answer")
        verbose_name_plural = _("answer plural")
        db_table = "Answer"

# Definicja modelu TrainingImage w postaci klasy


class TrainingImage(Model):

    titleImage = CharField(max_length=255, verbose_name=_(
        "title"), help_text=_("write title of image"), blank=False, null=True)

    def image_directory_path(instance, filename):
        ext = filename.split(".")[1]
        return 'images/szkolenie/{0}.{1}'.format(instance.titleImage, ext)

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Training image acc'))

    image = FileField(upload_to=image_directory_path, help_text=_("read image"),
                      verbose_name=_("Image"), validators=[validate_file_extension, file_size], null=True)
    date = DateTimeField(auto_now_add=True, verbose_name=_(
        "Date"), help_text=_("write date"))

    def __str__(self):
        return f"{self.titleImage}"

    class Meta:
        verbose_name_plural = _("Gallery training Plural")
        verbose_name = _("Gallery training Odmiana")
        ordering = [F('date').desc(nulls_last=True)]
        db_table = "Training images gallery"

# Definicja modelu QuestionImage w postaci klasy


class QuestionImage(Model):

    titleImage = CharField(max_length=255, verbose_name=_(
        "title"), blank=False, null=True, help_text=_("write title of image"))

    def image_directory_path(instance, filename):
        ext = filename.split(".")[1]
        return 'images/pytanie/{0}.{1}'.format(instance.titleImage, ext)

    image = FileField(upload_to=image_directory_path, help_text=_("read image"),
                      verbose_name=_("Image"), validators=[validate_file_extension, file_size], null=True)

    date = DateTimeField(auto_now_add=True, verbose_name=_(
        "date"), help_text=_("write date"))

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Question image acc'))

    def __str__(self):
        return f"{self.titleImage}"

    def __repr__(self):
        return "{self.id}, {self.titleImage}"

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = _("Gallery training questions plural")
        verbose_name = _("Gallery training questions odmiana")
        ordering = [F('date').desc(nulls_last=True)]
        db_table = "Question images gallery"

# Definicja modelu GaleryImage w postaci klasy


class GaleryImage(Model):

    name = TextField(verbose_name=_("description"), help_text=_(
        "DescriptionHelper"), null=True, blank=False)

    date = DateTimeField(auto_now_add=True, verbose_name=_(
        "date"), help_text=_("write date"))

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Galery image acc'))

    def image_directory_path(instance, filename):
        ext = filename.split(".")[1]

        date_time = instance.date.strftime("%m-%d-%Y-%H:%M:%S")

        return 'images/szkolenie/galeria/{0}.{1}'.format(date_time, ext)

    image = FileField(upload_to=image_directory_path, help_text=_("read image"),
                      verbose_name=_("Image"), validators=[validate_file_extension, file_size], null=True)

    sequence = PositiveIntegerField(verbose_name=_(
        "sequence"), help_text=_("set sequence image"), default=1)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ['sequence']
        verbose_name_plural = _("Gallery image plural")
        verbose_name = _("Gallery image")
        db_table = "GaleryImage"

# Definicja modelu Question w postaci klasy


class Question(Model):

    content = TextField(verbose_name=_("content"), help_text=_(
        "write question content"), blank=False, null=True)
    language = CharField(
        max_length=2,
        choices=Language.choices,
        default='PL',
        verbose_name=_("Language"),
        help_text=_("Choose language")
    )
    image = ForeignKey(
        QuestionImage,
        related_name="question",
        on_delete=CASCADE,
        blank=True,
        null=True,
        verbose_name=_("choose image"),
        help_text=_("choose image from list"),
    )

    sequence = PositiveIntegerField(verbose_name=_(
        "sequence"), help_text=_("set sequence question"), default=1)
    answers = ManyToManyField(Answer, through='QuestionsAnswersSet',
                              related_name="questions_Answers", help_text=_("Choose users"))

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Pytanie acc'))

    @property
    def numOfAnswers(self):
        return Question.objects.filter(id=self.id).annotate(num_answers=Count('answers')).values('num_answers').get().get('num_answers')

    def __str__(self):
        return f"{self.content}"

    class Meta:
        ordering = ['id']
        unique_together = ["content", "language"]
        verbose_name_plural = _("question plural")
        verbose_name = _("questionOdm")
        db_table = "Question"

# Definicja modelu Training w postaci klasy


class Training(Model):

    name = CharField(max_length=255, verbose_name=_("name"), help_text=_(
        "write_name_training"), blank=False, null=True)
    time = PositiveIntegerField(verbose_name=_(
        "Time"), help_text=_("Write time"), default=1, validators=[validate_training_days])

    obligatory = BooleanField(default=False, verbose_name=_(
        "Obligatory"), help_text=_("check obligatory"))

    language = CharField(
        max_length=2,
        choices=Language.choices,
        default='PL',
        verbose_name=_("Language"),
        help_text=_("Choose language")
    )

    image = ForeignKey(
        TrainingImage,
        related_name="training",
        on_delete=CASCADE,
        blank=True,
        null=True,
        verbose_name=_("choose image"),
        help_text=_("choose image from list"),
    )

    gallery = ManyToManyField(to=GaleryImage, verbose_name=_(
        "Gallery"), related_name="Training_gallery", help_text=_("Choose images"))
    questions = ManyToManyField(to=Question, related_name="Training_questions", verbose_name=_(
        "sequence"), help_text=_("Choose questions"))
    users = ManyToManyField(User, related_name="trainings_users",
                            through='CompletedTraining', help_text=_("Choose users"))

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Training acc'))

    @property
    def numOfGuests(self):
        return Training.objects.filter(id=self.id).annotate(num_users=Count('users')).values('num_users').get().get('num_users')

    @property
    def numOfQuestions(self):
        return Training.objects.filter(id=self.id).annotate(num_questions=Count('questions')).values('num_questions').get().get('num_questions')

    @property
    def numOfPictures(self):
        return Training.objects.filter(id=self.id).annotate(num_pictures=Count('gallery')).values('num_pictures').get().get('num_pictures')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ['time']
        verbose_name_plural = _("Training plural")
        verbose_name = _("TrainingOdm")
        db_table = "Training"

    @property
    def time_str(self):
        return f"{self.time} " + _("day") if self.time == 1 else \
            f"{self.time} " + _("days")


class CompletedTraining(Model):

    guest = ForeignKey(User, related_name="guests", on_delete=CASCADE, help_text=_(
        "Choose user"), verbose_name=_("GuestList"), null=True)
    training = ForeignKey(Training, related_name="completed_trainings", on_delete=CASCADE, help_text=_(
        "Choose training"), verbose_name=_("Training"), null=True)
    completed_date = DateField(verbose_name=_("Completed training"), help_text=_(
        "Date of completed training"), default=datetime.now)

    unexpirated = CompletedTrainingManager()
    _expiration_date = None

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Completed training acc'))

    @property
    def expiration_date(self):
        self.training.time = timedelta(days=self.training.time)
        return (self.completed_date + self.training.time)

    @expiration_date.setter
    def expiration_date(self, value):
        self._expiration_date = value

    def __str__(self):
        return f"Szkolenie: {self.training}, Osoba: {self.guest}"

    class Meta:
        verbose_name_plural = _("Completed training plural")
        verbose_name = _("Completed training odmiana")
        unique_together = ['guest', 'training']
        db_table = "Completed trainings"

# Definicja modelu QuestionsAnswersSet w postaci klasy


class QuestionsAnswersSet(Model):

    question = ForeignKey(Question, on_delete=CASCADE, help_text=_(
        "QuestionSet"), verbose_name=_("Question"), blank=True, null=True)
    answer = ForeignKey(Answer, on_delete=CASCADE, help_text=_(
        "AnswerSet"), verbose_name=_("Answer"), null=True, blank=True)
    status = BooleanField(default=False, verbose_name=_(
        "Status"), help_text=_("StatusSet"))

    @classmethod
    def accusative_case_verbose_name(cls):
        # You'll define the Polish translation as soon as you run makemessages
        return str(_('Question answers set acc'))

    def __str__(self):
        return f"Pytanie: {self.question}, Odpowiedź: {self.answer}, Status: {self.status}"

    class Meta:
        ordering = ['id']
        verbose_name_plural = _("Q&A set plural")
        verbose_name = _("Q&A set odmiana")
        unique_together = ['question', 'answer', 'status']
        db_table = "QuestionsAnswersSet"

# Dodawanie modelu proxy w celu możliwości ustawienia parametrów dodanych jako inline na język zrozumiały dla odbiorcy


class TrainingQuestionProxy(Training.questions.through):

    class Meta:
        proxy = True

    def __str__(self):
        return f"Treść: {self.question.content}, Kolejność: {self.question.sequence}"


class TrainingGalleryProxy(Training.gallery.through):

    class Meta:
        proxy = True

    def __str__(self):
        return f"Treść: {self.galeryimage.name}, Kolejność: {self.galeryimage.sequence}"


# Dodawanie sygnałów umożliwających usuwanie obrazów z katalogu media

@receiver(pre_delete, sender=TrainingImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)


@receiver(pre_delete, sender=GaleryImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)


@receiver(pre_delete, sender=QuestionImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)


@receiver(post_save, sender=TrainingImage)
def update_trainingimage_path(sender, instance, created, **kwargs):
    if created:
        imagefile = instance.image
        old_name = imagefile.name
        if not old_name:
            return
        new_name = os.path.basename(old_name)
        new_path = os.path.join(
            settings.MEDIA_ROOT, sender.image_directory_path(instance, new_name))
        if not os.path.exists(new_path):
            try:
                os.makedirs(os.path.dirname(new_path))
            except FileExistsError:
                time.sleep(0.001)
            except:
                raise
        os.rename(imagefile.path, new_path)
        instance.image.name = sender.image_directory_path(instance, new_name)
        instance.save()


@receiver(post_save, sender=QuestionImage)
def update_questionimage_path(sender, instance, created, **kwargs):
    if created:
        imagefile = instance.image
        old_name = imagefile.name
        if not old_name:
            return
        new_name = os.path.basename(old_name)
        new_path = os.path.join(
            settings.MEDIA_ROOT, sender.image_directory_path(instance, new_name))
        if not os.path.exists(new_path):
            try:
                os.makedirs(os.path.dirname(new_path))
            except FileExistsError:
                time.sleep(0.001)
            except:
                raise
        os.rename(imagefile.path, new_path)
        instance.image.name = sender.image_directory_path(instance, new_name)
        instance.save()


@receiver(post_save, sender=GaleryImage)
def update_galleryimage_path(sender, instance, created, **kwargs):
    if created:
        imagefile = instance.image
        old_name = imagefile.name
        if not old_name:
            return
        new_name = os.path.basename(old_name)
        new_path = os.path.join(
            settings.MEDIA_ROOT, sender.image_directory_path(instance, new_name))
        if not os.path.exists(new_path):
            try:
                os.makedirs(os.path.dirname(new_path))
            except FileExistsError:
                time.sleep(0.001)
            except:
                raise
        os.rename(imagefile.path, new_path)
        instance.image.name = sender.image_directory_path(instance, new_name)
        instance.save()

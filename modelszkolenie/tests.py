
from .admin import my_admin_site
from .models import *
from .views import *
from django.apps.registry import Apps
from django.contrib.auth.models import User as AuthUser, Group as AuthGroup
from django.db import models
from django.test import Client, TestCase, SimpleTestCase, TransactionTestCase, RequestFactory, override_settings
from django.urls import resolve
from datetime import datetime, date
from json import dumps
from pickle import dumps, loads
from rest_framework.status import HTTP_204_NO_CONTENT, HTTP_201_CREATED, HTTP_200_OK
from rest_framework.test import APITestCase


class TestsThatDependsOnPrimaryKeySequences(TransactionTestCase):
    reset_sequences = True

    def test_user_pk(self):

        user1 = User.objects.create(first_name="Michał", last_name="Bodura")
        # user1.pk is guaranteed to always be 1
        self.assertEqual(user1.pk, 1)


class TestsThatDependsOnForeignKeySequences(TransactionTestCase):

    def test_instance(self):

        c1 = Company(name="Atos", address="Testowa 54")
        c1.save()
        c2 = Company(name="IU Techology", address="Dubois 114/116")
        c2.save()
        self.assertIsInstance(c1, c2)

    def test_fk(self):

        c1 = Company(name="Atos", address="Testowa 54")
        c1.save()
        c2 = Company(name="IU Techology", address="Dubois 114/116")
        c2.save()
        u1 = User(first_name='John', last_name='Smith',
                  email='john@example.com', company=c1)
        u1.save()
        u2 = User(first_name='Paul', last_name='Jones',
                  email='paul@example.com')
        u2.save()

        self.assertEqual(u1.company.name, "Atos")


class UserBasicTest(TestCase):

    def setUp(self):

        self.user = User()
        self.user.first_name = "Michał"
        self.user_last_name = "Bodura"
        self.user.save()

    def setUpClient(self):

        self.client = Client()

    def test_details(self):

        response = self.client.get('/admin')
        self.assertEqual(response.status_code, 301)

    def create_user(self, first_name="Michał", last_name="Bodura"):

        return User.objects.create(first_name=first_name, last_name=last_name)

    def test_fields(self):

        user = User()
        user.first_name = "Michał"
        user.last_name = "Badura"
        user.save()
        record = User.objects.get(pk=user.id)
        self.assertEqual(record, user)

    def test_user_creation(self):

        u = self.create_user()
        self.assertIsInstance(u, User)

    def test_greater_than_zero(self):

        u1 = self.setUp()
        u2 = self.setUp()
        users = [u1, u2]
        self.assertGreater(len(users), 0)


class TestModelDefinition(SimpleTestCase):

    def test_model_definition(self):
        test_apps = Apps(['modelszkolenie'])

        class TestModel(models.Model):
            class Meta:
                apps = test_apps

        self.assertNull()


class CompletedTrainingTestCase(TestCase):

    def setUser(self):

        self.user = User()
        self.user.first_name = "Michał"
        self.user.last_name = "Bodura"
        self.user.save()
        return self.user

    def setSecondUser(self):

        self.user = User()
        self.user.first_name = "Piotr"
        self.user.last_name = "Słowik"
        self.user.save()
        return self.user

    def setTraining(self):

        self.training = Training()
        self.training.name = "Szkolenie z dokera"
        self.training.time = 14
        self.training.obligatory = True
        self.training.save()
        return self.training

    def setUp(self):

        user = self.setUser()
        training = self.setTraining()
        self.ct = CompletedTraining()

        self.ct.guest = user
        self.ct.training = training
        self.ct.completed_date = "2021-08-17"
        self.ct.save()
        user = self.setSecondUser()
        training = self.setTraining()
        self.ct = CompletedTraining()

        self.ct.guest = user
        self.ct.training = training
        self.ct.completed_date = "2021-08-31"
        self.ct.save()

        return self.ct

    def test_date(self):

        queryset = CompletedTraining.objects.filter(completed_date__year=2021)
        print(queryset.count())
        self.assertGreater(queryset.count(), 0)

    def test_completedTrainingTwoPearson(self):

        queryset = CompletedTraining.objects.filter(
            completed_date__gte='2021-08-15')
        self.assertGreaterEqual(queryset.count(), 2)

    def test_completedTrainingAlmostEquals(self):

        queryset = CompletedTraining.objects.filter(
            completed_date__gte='2021-08-15')
        self.assertAlmostEquals(queryset.count(), 2)

    def test_completedTrainingNotAlmostEquals(self):

        queryset = CompletedTraining.objects.filter(
            completed_date__gte=datetime.today())
        self.assertNotAlmostEquals(queryset.count(), 2)

    def test_completedTrainingExcluded(self):

        queryset = CompletedTraining.objects.exclude(
            completed_date__gte=datetime.today())
        self.assertAlmostEquals(queryset.count(), 1)


class UserTestCase(TestCase):

    def test_fields(self):

        user = User(id=4, first_name="Michał", last_name="Bodura")
        user.save()
        record = User.objects.create(
            id=5, first_name="Michał", last_name="Bodura")
        self.assertEqual(record.first_name, user.first_name)

    def setUp(self):

        User.objects.create(first_name="Michal", last_name="Bodura")
        User.objects.create(email="piotr.nowak@iutechnology.pl")

    # Sprawdzenie poprawnosci modelu pod wzgledem atrybutu - poprawna name atrybutu

    def test_attributes(self):

        user1 = User.objects.get(first_name="Michal")
        user2 = User.objects.get(email="piotr.nowak@iutechnology.pl")
        queryset = [user1, user2]
        self.assertTrue(queryset, "Cos nie poszlo z atrybutami")

    # Sprawdzanie nazwy atrybutu - bledna name atrybutu

    def test_attributes2(self):

        user1 = User.objects.get(first_name="Michal")
        user2 = User.objects.get(email="piotr.nowak@iutechnology.pl")
        queryset = [user1, user2]
        self.assertTrue(queryset, "Cos nie poszlo z atrybutami")

    def test_ifPiotrNowakExists(self):

        User.objects.create(first_name="Piotr", last_name="Nowak")
        ex1 = User.objects.get(first_name="Piotr", last_name="Nowak")
        self.assertTrue(ex1, "Nie ma Piotra Nowaka na liscie")

    def test_ifMichalIsInList(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura")
        queryset = User.objects.filter(first_name="Michal")
        self.assertTrue(
            queryset, "Nie ma uzytkownika o first_nameniu Michal na liscie")

    # False
    def test_ifUsersEquals_one(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura")
        self.assertEquals(user1, user2, "Nie sa rowne")

    # True
    def test_ifUsersNotEquals(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura",
                                    email="michal_bodura@iutechnology.pl", id_card="CDJ757557")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura",
                                    email="michal_bodura@iutechnology.pl", id_card="CDJ757557")
        self.assertNotEquals(
            user1, user2, "Istnieje redundacja w bazie, prosze to zweryfikowac")

    def test_ifIdCardUnique(self):
        user1 = User.objects.create(id_card="CDJ757557")
        user2 = User.objects.create(id_card="CDJ757558")
        self.assertNotEquals(
            user1, user2, "Numery dowodu nie są unikalne")

    # Sprawdzam typ zmiennej
    def test_type(self):

        User.objects.create(first_name="Piotr", last_name="Nowak")
        ex1 = User.objects.get(first_name="Piotr", last_name="Nowak")
        self.assertEqual(ex1.first_name + " " + ex1.last_name, 'Piotr Nowak')

    # Sprawdzam, czy dane wyświetlają się w bazie

    def test_user_data(self):

        namesUser = []
        for u in User.objects.all():
            namesUser.append(u.first_name)
        self.assertEqual(len(namesUser), 2)

    def test_unique_user_data(self):

        namesUser = []
        for u in User.objects.all():
            if u not in namesUser:
                namesUser.append(u.first_name)
        self.assertEqual(len(namesUser), 2)

    # Sprawdzam czy istnieje w bazie pracownik, który pracuje w firmie IU Technology

    def test_if_worker_in_IU_Technology(self):

        company1 = Company.objects.create(
            name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",
                                    email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        self.assertEqual(user1.company.name, "IU Technology")

    def test_dict_object(self):

        company1 = Company.objects.create(
            name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",
                                    email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        qs = User.objects.values_list('first_name', 'last_name')
        reloaded_qs = User.objects.all()
        reloaded_qs.query = loads(dumps(qs.query))
        dictObject = {'first_name': 'Testowy', 'last_name': 'testowy'}
        self.assertEqual(reloaded_qs[2], dictObject)

    def test_JSON_object(self):

        company1 = Company.objects.create(
            name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",
                                    email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        qs = User.objects.values_list('first_name', 'last_name')
        reloaded_qs = User.objects.all()
        reloaded_qs.query = loads(dumps(qs.query))
        dictObject = dumps({'first_name': 'Testowy', 'last_name': 'testowy'})
        self.assertJSONEqual(dumps(reloaded_qs[2]), dictObject)


class CompanyTestCase(TestCase):

    def test_ifNotEquals(self):

        company1 = Company.objects.create(
            name="IU Technology", address="Dubois 114/116")
        company2 = Company.objects.create(
            name="IU Technology", address="Dubois 112")
        self.assertNotEquals(company1, company2)


class TrainingTestCase(TestCase):

    def test_ifTrainingStartsSeptember(self):
        d = date.today()


class TestUrls(TestCase):
    def test_resolution_for_user(self):
        resolver = resolve('/api/user/47')
        self.assertEqual(resolver.func.cls, UserDetail)

    def test_resolution_for_all_users(self):
        resolver = resolve('/api/user/')
        self.assertEqual(resolver.func.cls, UserList)

    def test_admin(self):
        self.assertTrue(my_admin_site.urls)


# Create your tests here.


# Test sprawdzające, czy dany użytkownik należący do groupy Administrator ma uprawnienie do logowania się do poszczególnych serwisów

class GroupPermissionsTests(TestCase):

    def setUp(self):
        # create permissions group
        group_name = "Administrator"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.user = AuthUser.objects.create_user(
            username="michal.darowny", email="michal.bodura@iutechnology.pl", password="Monitor2021")
        self.c = Client()

    def tearDown(self):
        self.user.delete()
        self.group.delete()

    def test_user_cannot_access(self):
        """user NOT in group should not have access
        """
        self.c.login(username='test', password='test')
        response = self.c.get("http://10.9.9.75:8000/api/v1/training/")
        self.assertEqual(response.status_code, 403,
                         u'user in group should have access')

    def test_user_can_access(self):
        """user in group should have access
        """
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')
        response = self.c.get("http://10.9.9.75:8000/api/v1/training/")
        self.assertEqual(response.status_code, 200,
                         u'user in group should have access')


class GroupFailedPermissionsTests(TestCase):

    def setUp(self):
        # create permissions group
        group_name = "Ochrona"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = AuthUser.objects.create_user(
            username="test", email="test@test.com", password="test")

    def tearDown(self):
        self.user.delete()
        self.group.delete()

    def test_user_cannot_access(self):
        """user NOT in group should not have access
        """
        self.c.login(username='test', password='test')
        response = self.c.get("http://10.9.9.75:8000/api/v1/training/")
        self.assertEqual(response.status_code, 403,
                         u'user in group should have access')

    def test_user_can_access(self):
        """user in group should have access
        """
        self.user.groups.add(self.group)
        self.user.save()
        self.c.login(username='test', password='test')
        response = self.c.get("http://10.9.9.75:8000/api/v1/training/")
        self.assertEqual(response.status_code, 403,
                         u'user in group should have access')


# Sprawdza, czy strona django zwraca zapytanie, czyli należy do RequestFactory
class TestBelongsToRequestFactory(TestCase):
    def setUp(self):

        self.factory = RequestFactory()
        self.user = AuthUser.objects.create_user(
            username='michal.bodura', email='michal.bodura@iutechnology.pl', password='Monitor2021')

    def test_details(self):
        # Create an instance of a GET request.
        request = self.factory.get('http://10.9.9.75:8000/api/v1/user')

        request.user = self.user

        request.user = CustomAbstractUser()
        # Use this syntax for class-based views.
        response = UserList.as_view()(request)
        self.assertEqual(response.status_code, 200)

# Sprawdza działanie metody get_context_data(), który sprawdza, czy może przekazać żądanie do metody setup()


class HomePageTest(TestCase):
    def test_environment_set_in_context(self):
        request = RequestFactory().get('/')
        view = HomeView()
        view.setup(request)

        context = view.get_context_data()
        self.assertIn('environment', context)

    def test_environment_get(self):
        request = RequestFactory().get('/api/v1/user')
        view = CompanyList()
        view.setup(request)

        context = view.get(request)
        self.assertIn('environment', context)


class SearchFormTestCase(TestCase):
    def test_empty_get(self):
        response = self.client.get('', HTTP_HOST='10.9.9.75:8000')
        self.assertEqual(response.status_code, 200)


class MultiDomainTestCase(TestCase):
    @override_settings(ALLOWED_HOSTS=['otherserver'])
    def test_other_domain(self):
        response = self.client.get('http://otherserver/foo/bar/')


class TestsThatDependsOnPrimaryKeySequences(TransactionTestCase):
    reset_sequences = True

    def test_user_pk(self):
        user1 = User.objects.create(
            first_name="Michał", last_name="Bodura", id_card="KOU75382")
        # lion.pk is guaranteed to always be 1
        self.assertEqual(user1.pk, 1)


class TestClientAccessToAdminPanel(TestCase):
    def test_access(self):
        c = Client()
        response = c.get(
            '/admin/', {'username': 'john', 'password': 'smith'}, follow=True)
        self.assertEqual(response.status_code, 200)


class UserListTestCase(APITestCase):
    def setUp(self):
        group_name = "Administrator"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = AuthUser.objects.create_user(
            username="michal.darowny", email="michal.bodura@iutechnology.pl", password="Monitor2021")
        self.group.user_set.add(self.user)
        self.user.groups.add(self.group)
        self.user.save()
        self.user = User.objects.create(id=34, id_card="fkdsjgk")
        self.user2 = User.objects.create(
            id=12, first_name="Michał", last_name="fdsg", email="test@test.com", id_card="adarki")

    def login(self):
        self.c.login(username='michal.darowny', password='Monitor2021')

    def test_poster(self):

        self.login()
        data = {"first_name": "Michał", "last_name": "",
                "email": "", "id_card": "KOO75214"}
        response = self.c.post(
            "http://10.9.9.75:8000/api/v1/user", data=data, format='json')
        self.assertEqual(response.status_code, HTTP_201_CREATED)

    def test_putter(self):
        self.login()
        data = {
            "first_name": "Michał",
            "last_name": "afs",
            "full_name": "Michał afs",
            "email": "Stary@stary.com",
            "id_card": "adarki"
        }
        response = self.c.put(
            "http://10.9.9.75:8000/api/v1/user/12", data, content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_deleter(self):
        self.login()
        response = self.c.delete(
            "http://10.9.9.75:8000/api/v1/user/12", content_type='application/json')
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)

    def test_retriever(self):
        self.login()
        response = self.c.get("http://10.9.9.75:8000/api/v1/user/34")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_getter(self):
        self.login()
        response = self.c.get("http://10.9.9.75:8000/api/v1/user")
        self.assertEqual(response.status_code, HTTP_200_OK)


class CompanyListTestCase(APITestCase):
    def setUp(self):
        group_name = "Administrator"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = AuthUser.objects.create_user(
            username="michal.darowny", email="michal.bodura@iutechnology.pl", password="Monitor2021")
        self.group.user_set.add(self.user)
        self.user.groups.add(self.group)
        self.user.save()
        self.company = Company.objects.create(id=42, name="LIFJSo")
        self.company2 = Company.objects.create(
            id=43, name="jakas firma", address="Zamenhofa 3", city="Łódź")

    def login(self):
        self.c.login(username='michal.darowny', password='Monitor2021')

    def test_poster(self):

        self.login()
        data = {"name": "IU Technology",
                "address": "Dubois 114/116", "city": "Łódź"}

        response = self.c.post(
            "http://10.9.9.75:8000/api/v1/company", data=data, format='json')
        self.assertEqual(response.status_code, HTTP_201_CREATED)

    def test_retriever(self):
        self.login()

        response = self.c.get("http://10.9.9.75:8000/api/v1/company/42")

        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_getter(self):

        self.login()

        response = self.c.get("http://10.9.9.75:8000/api/v1/company")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_putter(self):
        self.login()
        data = {
            "name": "dsad",
            "address": "Zammenhofa 1",
            "city": "Łódź"
        }
        response = self.c.put("http://10.9.9.75:8000/api/v1/company/43",
                              data=data, content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_deleter(self):
        self.login()
        response = self.c.delete(
            "http://10.9.9.75:8000/api/v1/company/43", content_type='application/json')
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)


class QuestionListTestCase(APITestCase):
    def setUp(self):
        group_name = "Administrator"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = AuthUser.objects.create_user(
            username="michal.darowny", email="michal.bodura@iutechnology.pl", password="Monitor2021")
        self.group.user_set.add(self.user)
        self.user.groups.add(self.group)
        self.user.save()
        self.question = Question.objects.create(id=52, content="fjdois")
        self.question2 = Question.objects.create(
            id=8, content="Czy jest dobrze?", sequence=2, language="PL", image=None)

    def login(self):
        self.c.login(username='michal.darowny', password='Monitor2021')

    def test_poster(self):

        self.login()
        data = {"content": "fjkdsjgkdjsgkd", "sequence": 5, "language": "PL"}

        response = self.c.post(
            "http://10.9.9.75:8000/api/v1/question", data=data, format='json')
        self.assertEqual(response.status_code, HTTP_201_CREATED)

    def test_retriever(self):
        self.login()

        response = self.c.get("http://10.9.9.75:8000/api/v1/question/52")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_getter(self):

        self.login()
        response = self.c.get("http://10.9.9.75:8000/api/v1/question")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_putter(self):
        self.login()
        data = {
            "content": "Czy jest dobrze?",
            "sequence": 2,
            "language": "PL",
            "image": None
        }
        response = self.c.put("http://10.9.9.75:8000/api/v1/question/8",
                              data=data, content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_deleter(self):
        self.login()

        response = self.c.delete(
            "http://10.9.9.75:8000/api/v1/question/8", content_type='application/json')
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)


class TrainingListTestCase(APITestCase):
    def setUp(self):
        group_name = "Administrator"
        self.group = AuthGroup(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = AuthUser.objects.create_user(
            username="michal.darowny", email="michal.bodura@iutechnology.pl", password="Monitor2021")
        self.group.user_set.add(self.user)
        self.user.groups.add(self.group)
        self.user.save()
        self.training = Training.objects.create(id=34, name="fkdsjgk")
        self.training2 = Training.objects.create(
            id=35, name="Opieka", time=39, obligatory=True, language="EN")

    def login(self):
        self.c.login(username='michal.darowny', password='Monitor2021')

    def test_poster(self):
        self.login()
        data = {
            "name": "Opoka",
            "time": 14,
            "obligatory": False,
            "language": "PL"
        }

        response = self.c.post(
            "http://10.9.9.75:8000/api/v1/training", data=data, format='json')
        self.assertEqual(response.status_code, HTTP_201_CREATED)

    def test_getter(self):
        self.login()
        response = self.c.get("http://10.9.9.75:8000/api/v1/training")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_retriever(self):
        self.login()
        response = self.c.get("http://10.9.9.75:8000/api/v1/training/34")
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_putter(self):
        self.login()
        data = {
            "name": "Szkolenie Robocze",
            "time": 25,
            "obligatory": True,
            "language": "PL"
        }
        response = self.c.put("http://10.9.9.75:8000/api/v1/training/35",
                              data=data, content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_deleter(self):
        self.login()
        response = self.c.delete(
            "http://10.9.9.75:8000/api/v1/training/35", content_type='application/json')
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)

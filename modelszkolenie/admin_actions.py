from django.contrib.admin import action as Action
from django.contrib.messages import SUCCESS, success
from django.utils.translation import ngettext, gettext_lazy as _


# Definicja funkcji odpowiadającej za wyrażenie jęzkowe

class AdminActions():

    def get_polish_plural_expr(num_records):
        return ((num_records == 1) or (
            (num_records % 10 >= 2 and num_records % 10 <= 4))) and (int(num_records/10) != 1)

    def get_custom_message_user(self, request, num_records, status, text1, text2):

        return self.message_user(request, ngettext(
            '%d ' + _(text1),
            '%d ' + _(text2),
            num_records,
        ) % num_records, status)

    def get_polish_custom_message_user(self, request, num_records, expression, status, text1, text2, text3):

        return self.message_user(request, ngettext(
            '%d ' + _(text1),
            '%d ' + _(text2),
            num_records,
        ) % num_records, status) if expression else \
            self.message_user(request, ngettext(
                '%d ' + _(text1),
                '%d ' + _(text3),
                num_records,
            ) % num_records, status)

    def success_delete_custom_message_user(request, num_records, text1, text2):
        return success(request, ngettext(
            '%d ' + _(text1),
            '%d ' + _(text2),
            num_records,
        ) % num_records)

    # Wykonanie akcji ustawiającących dane dla klas w panelu administracyjnym dla pola status

    @Action(description=_('Change to good'))
    def mark_correct(self, request, queryset):
        updated = queryset.update(status=True)
        self.get_custom_message_user(self, request, updated, SUCCESS,
                                     text1='answer changed to correct.', text2='answers changed to correct.')

    @Action(description=_('Change to wrong'))
    def mark_uncorrect(self, request, queryset):
        updated = queryset.update(status=False)
        self.get_custom_message_user(self, request, updated, SUCCESS,
                                     text1='answer changed to uncorrect.', text2='answers changed to uncorrect.')

    # Wykonanie akcji ustawiającących dane dla klas w panelu administracyjnym dla pola obligatory

    @Action(description=_('Change to obligatory'))
    def mark_obligatory(self, request, queryset):

        updated = queryset.update(obligatory=True)
        polish_plural_expr = self.get_polish_plural_expr(updated)

        return self.get_polish_custom_message_user(self, request, updated, polish_plural_expr,
                                                   SUCCESS, text1='training changed to obligatory.', text2='trainings changed to obligatory.',
                                                   text3='trainings pro changed to obligatory.')

    @Action(description=_('Change to unobligatory'))
    def mark_unobligatory(self, request, queryset):
        updated = queryset.update(obligatory=False)
        polish_plural_expr = self.get_polish_plural_expr(updated)

        return self.get_polish_custom_message_user(self, request, updated, polish_plural_expr,
                                                   SUCCESS, text1='training changed to unobligatory.', text2='trainings changed to unobligatory.',
                                                   text3='trainings changed to unobligatory.')

    # Wykonanie akcji ustawiającących dane dla klas w panelu administracyjnym dla pola language

    @Action(description=_('Change to english'))
    def set_english(self, request, queryset):

        updated = queryset.update(language='EN')
        polish_plural_expr = self.get_polish_plural_expr(updated)

        return self.get_polish_custom_message_user(self, request, updated, polish_plural_expr,
                                                   SUCCESS, text1='record changed to english.', text2='records changed to english.',
                                                   text3='records pro changed to english.')

    @Action(description=_('Change to polish'))
    def set_polish(self, request, queryset):
        updated = queryset.update(language='PL')
        polish_plural_expr = self.get_polish_plural_expr(updated)

        return self.get_polish_custom_message_user(self, request, updated, polish_plural_expr,
                                                   SUCCESS, text1='record changed to polish.', text2='records changed to polish.',
                                                   text3='records pro changed to polish.')

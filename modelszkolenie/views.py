from .models import *
from .permissions import *
from .serializers import *
from datetime import date
from django_filters.rest_framework import DjangoFilterBackend
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.shortcuts import render
from django.views.generic import TemplateView
from django.utils.translation import gettext_lazy as _
from django.template import RequestContext
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_200_OK

# def home(request):
#     return render(request, 'modelszkolenie/home.html')

class HomeView(TemplateView):
    template_name = 'modelszkolenie/home.html'

    def get_context_data(self, **kwargs):
        kwargs['environment'] = 'Production'
        return super().get_context_data(**kwargs)

def page_not_found(request):

    response = render('404.html', context_instance=RequestContext(request))
    response.status_code = 404
    return response


class CustomObtainAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(CustomObtainAuthToken, self).post(
            request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        return Response({'token': token.key, 'id': token.user_id})


class CompanyList(ListCreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Company.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = CompanySerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['name', 'address', 'city']
    search_fields = ['name', 'address', 'city']
    ordering_fields = ['name', 'address', 'city']
    ordering = ['name']

    def get_queryset(self):
        return self.queryset

    def get(self, request, *args, **kwargs):
        queryset = Company.objects.all()
        serializer = CompanySerializer(list(queryset), many=True)
        if queryset.count() == 0:
            return Response({str(_('Detail')): _("List of companies is empty")}, status=HTTP_200_OK)
        else:
            return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = CompanySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({str(_('Detail')): _("Added company sucessfully Rest API")}, status=HTTP_201_CREATED)
        return Response({str(_('Detail')): _("Added company unsucessfully Rest API")}, status=HTTP_400_BAD_REQUEST)

# Definicja klasy odpowiadająca za stworzenie widoku API dla metod PUT, PATCH, DELETE (usuwanie, edycja danych)
# w modelu Company


class CompanyDetail(RetrieveUpdateDestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Company.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = CompanySerializer

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This company was not found in the database')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)

# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET i POST (pobieranie, dodawanie danych) dla modelu User


class UserList(ListCreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = User.objects.select_related("company")
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['first_name', 'last_name', 'email',
                     'id_card', 'country', 'company__name', 'company__city']
    search_fields = ['first_name', 'last_name', 'email',
                     'id_card', 'country', 'company__name', 'company__city']
    ordering_fields = ['last_name', 'first_name', 'company__name']
    ordering = [F('last_name').asc(nulls_last=True),
                F('first_name').asc(nulls_last=True)]

    def get(self, request, *args, **kwargs):
        queryset = User.objects.select_related("company")
        serializer = UserSerializer(list(queryset), many=True)
        if queryset.count() == 0:
            return Response({str(_('Detail')): _("List of guests is empty")}, status=HTTP_200_OK)
        else:
            return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({str(_('Detail')): _("Added guest sucessfully Rest API")}, status=HTTP_201_CREATED)
        return Response({str(_('Detail')): _("Added guest unsucessfully Rest API")}, status=HTTP_400_BAD_REQUEST)

# Definicja klasy odpowiadająca za stworzenie widoku API dla metod PUT, PATCH, DELETE (usuwanie, edycja danych)
# w modelu User


class UserDetail(RetrieveUpdateDestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This ID number was not found in the database - contact your security officer')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)

    queryset = User.objects.select_related("company")
    serializer_class = UserSerializer
    lookup_field = 'pk'


# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET i POST (pobieranie, dodawanie danych) dla modelu Training

class TrainingList(ListCreateAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Training.objects.prefetch_related("users", "questions")
    serializer_class = TrainingSerializer
    permission_classes = [IsAuthenticated, IsAdministrator]
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['name', 'time', 'obligatory']
    search_fields = ['name', 'time', 'language']
    ordering_fields = ['name', 'time']
    ordering = ['time']

    def get(self, request, *args, **kwargs):
        queryset = Training.objects.prefetch_related("users", "questions")
        serializer = TrainingSerializer(list(queryset), many=True)
        if queryset.count() == 0:
            return Response({str(_('Detail')): _("List of trainings is empty")}, status=HTTP_200_OK)
        else:
            return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = TrainingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({str(_('Detail')): _("Added training sucessfully Rest API")}, status=HTTP_201_CREATED)
        return Response({str(_('Detail')): _("Added training unsucessfully Rest API")}, status=HTTP_400_BAD_REQUEST)


# Definicja klasy odpowiadająca za stworzenie widoku API dla metod PUT, PATCH, DELETE (usuwanie, edycja danych)
# w modelu Training


class TrainingDetail(RetrieveUpdateDestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Training.objects.prefetch_related("users", "questions")
    serializer_class = TrainingSerializer
    permission_classes = [IsAuthenticated, IsAdministrator]

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This training name was not found in the database - contact your security officer')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)

# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET i POST (pobieranie, dodawanie danych) dla modelu Question


class QuestionList(ListCreateAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    serializer_class = QuestionSerializer
    permission_classes = [IsAdministrator, IsAuthenticated]
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ['content', 'language', 'answers__answer']
    search_fields = ['content', 'language', 'answers__answer']

    def get(self, request, *args, **kwargs):
        queryset = Question.objects.all()
        serializer = QuestionSerializer(list(queryset), many=True)
        if queryset.count() == 0:
            return Response({str(_('Detail')): _("List of questions is empty")}, status=HTTP_200_OK)
        else:
            return Response(serializer.data, status=HTTP_200_OK)

    def get_queryset(self):
        return Question.objects.prefetch_related("answers", "image")

    def post(self, request, *args, **kwargs):
        serializer = QuestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({str(_('Detail')): _("Added question sucessfully Rest API")}, status=HTTP_201_CREATED)
        return Response({str(_('Detail')): _("Added question unsucessfully Rest API")}, status=HTTP_400_BAD_REQUEST)


# Definicja klasy odpowiadająca za stworzenie widoku API dla metod PUT, PATCH, DELETE (usuwanie, edycja danych)
# w modelu Question


class QuestionDetail(RetrieveUpdateDestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Question.objects.prefetch_related("answers", "image")
    serializer_class = QuestionSerializer
    permission_classes = [IsAdministrator, IsAuthenticated]

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This question content was not found in the database')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)


# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET i POST (pobieranie, dodawanie danych) dla modelu Answer


class AnswerList(ListCreateAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Answer.objects.all()
    permission_classes = [IsAdministrator, IsAuthenticated]
    serializer_class = AnswerSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['answer']
    search_fields = ['answer']
    ordering_list = ['id', 'answer']
    ordering = ['id']

    def get(self, request, *args, **kwargs):
        queryset = Answer.objects.all()
        serializer = AnswerSerializer(list(queryset), many=True)
        if queryset.count() == 0:
            return Response({str(_('Detail')): _("List of answers is empty")}, status=HTTP_200_OK)
        else:
            return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = AnswerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({str(_('Detail')): _("Added answer sucessfully Rest API")}, status=HTTP_201_CREATED)
        return Response({str(_('Detail')): _("Added answer unsucessfully Rest API")}, status=HTTP_400_BAD_REQUEST)

# Definicja klasy odpowiadająca za stworzenie widoku API dla metod PUT, PATCH, DELETE (usuwanie, edycja danych)
# w modelu Answer


class AnswerDetail(RetrieveUpdateDestroyAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [IsAdministrator, IsAuthenticated]

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This answer was not found in the database')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)

# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET (pobieranie danych) dla modelu CompletedTraining


class TrainingUserList(ListAPIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    serializer_class = CompletedTraningSerializer
    queryset = CompletedTraining.unexpirated.prefetch_related(
        "guest", "training")
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['guest', 'training', 'completed_date']
    search_fields = ['guest', 'training']
    ordering_fields = ['guest__first_name',
                       'guest__last_name', 'training', 'completed_date']
    ordering = ['completed_date']

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This ID number was not found in the database - contact your security officer')},
                            status=HTTP_404_NOT_FOUND)

        return super(TrainingUserList, self).handle_exception(exc)

    def get_queryset(self):
        try:
            users = User.objects.get(pk=self.kwargs['pk'])
            # users = User.objects.get(card_slug=self.kwargs['card_slug'])
            return CompletedTraining.unexpirated.filter(guest=users, expiration_date__lt=date.today())
        except:
            self.handle_exception(Http404)

    def get(self, request, pk, *args, **kwargs,):
        try:
            users = User.objects.get(pk=self.kwargs['pk'])
            queryset = CompletedTraining.unexpirated.filter(
                guest=users, expiration_date__lt=date.today())
            serializer = CompletedTraningSerializer(list(queryset), many=True)

            if queryset.count() == 0:
                return Response({str(_('Detail')): _("List of important trainings is empty")}, status=HTTP_200_OK)
            else:
                return Response(serializer.data, status=HTTP_200_OK)
        except ObjectDoesNotExist:
            self.handle_exception(Http404)

# Definicja klasy odpowiadająca za stworzenie widoku API z ustawionymi parametrami, paginacją i filtrami
# z użyciem metod GET (pobieranie danych) dla modelu QuestionsAnswersSet


class QuestionsAnswerList(ListAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    serializer_class = QuestionsAnswersSetSerializer
    permission_classes = [IsAdministrator]
    queryset = QuestionsAnswersSet.objects.prefetch_related(
        "question", "answer")
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filter_fields = ['question', 'answer', 'status']
    search_fields = ['question__content', 'answer__answer', 'status']
    ordering_fields = ['question', 'answer', 'status']
    ordering = ['id']

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return Response({str(_('Detail')): _('This question was not found in the database')},
                            status=HTTP_404_NOT_FOUND)

        return super().handle_exception(exc)

    def get_queryset(self):
        try:
            asked_question = Question.objects.get(pk=self.kwargs['pk'])
            return QuestionsAnswersSet.objects.filter(question=asked_question)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, *args, **kwargs):
        try:
            asked_question = Question.objects.get(pk=self.kwargs['pk'])

            queryset = QuestionsAnswersSet.objects.filter(
                question=asked_question)
            serializer = QuestionsAnswersSetSerializer(
                list(queryset), many=True)

            if queryset.count() == 0:
                return Response({str(_('Detail')): _("List of questions and answers set is empty")}, status=HTTP_200_OK)
            else:
                return Response(serializer.data, status=HTTP_200_OK)
        except ObjectDoesNotExist:
            self.handle_exception(Http404)

=====
Django admin panel for booking trainings
=====

Szkolenia is a Django app to conduct Web-based trainings. 

Detailed documentation is in the "docs" directory.

Quick start

-----------

1. Install important packages to this model using pip install package. Important packages is located on requirements.txt with versions

2. Add "modelszkolenie" to your INSTALLED_APPS setting like this:

    INSTALLED_APPS = [
        ...
        'modelszkolenie',
    ]

3. Include the sample URLconf in your project urls.py like this:

    path('',views.home, name='home'),


4. You can run server on two ways:

-----------

=====
a. By docker
=====

4a. You should set SQL_HOST variable to ``db`` located in ``.env.dev.`` and ``.env`` files file which is connected to ``docker-compose.yml`` file

5a. If you want to build admin panel by using docker, you should build image with command ``docker-compose build``.

6a. Run container with django server and postgresql database by running ``docker-compose up`` 

7a. Create superuser to login to admin panel by running ``docker-compose run web python manage.py shell -c ``from django.contrib.auth.models import User; User.objects.create_superuser('user', 'user@user.pl', 'password')`` ``

-----------

=====
b. By django server
=====

4b. You should set SQL_HOST variable to ``localhost`` or ``127.0.0.1`` located in ``.env.dev.`` and ``.env`` files to connect django local server

5b. Run ``python manage.py migrate`` or ``dj m`` to create the modelszkolenie model

6b. Install postgresql and pgadmin4 software 

7b. Start the development server and visit http://127.0.0.1:8000/admin/ to create training. 

8b. Create superuser to login to admin panel by running ``python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('user', 'user@user.pl', 'password')"``  or  ``python manage.py createsuperuser``

9b. Run command ``python manage.py loaddata admin_interface_theme_modelszkolenie.json`` to load fixture/template to project
